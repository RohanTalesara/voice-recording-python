laMkES kannaDada KyAta sAhitigaLallobbaru hAgU laMkES patrike ya sthApaka saMpAdakaru
padavi paDeda baLika ivaru kannaDa da adhyApakarAgi sEve sallisi nivRuttarAdaru
immaDi pulikESi cAlukya vaMSada ati prasidda cakravarti
hIgAgi idaralli nimma sahAya bEku
idu jagattina ati saNNa rAZhTragaLa paiki oMdu
SivarAma kAraMtaru racisida kannaDa kAdaMbari
vEdagaLu BAratIya saMskRutiya atyaMta pramuKa graMthagaLu
BAZhe ya upayOgada oMdu kale
I jilleya hesaru beMgaLUru grAmAMtara
baLLAri karnATaka rAjyada oMdu jille idE hesarina nagara jilleya rAjadhAni
boMbAyina pravAsi tANagaLu hesarina lEKanavannu nIvu prAraMBisabahudu
BArata sarkAra hesarina lEKanavannu nIvu prAraMBisabahudu
AvaraNa KyAta sAhiti Bairappanavara ittIcina kAdaMbari
idu hiMdU dharma dalli pavitra mara eMdu parigaNitavAgide
BArata sarkAra padavannu bEre lEKanagaLalli huDuki
kannaDa BAZheyannu pramuKavAgi BAratada karnATaka rAjyadalli upayOgisalAguttade mattu svalpa maTTige akkapakkada rAjyagaLalli saha upayOgisalAguttade
I sthaLavannu nava bRuMdAvana eMdu kareyalAguttade
ivaru kannaDadalli ravicaMdran nirdESanada sipAyi mattu ji BAravi nirdESanada SrI maMjunAtha citragaLalli naTisiddAre
boMbAyina pravAsi tANagaLu padavannu bEre lEKanagaLalli huDuki
idu yurOpiyan okkUTa da sadasya rAZhTravAgide
BAratada sEnA puraskAragaLu hesarina lEKanavannu nIvu prAraMBisabahudu
ivara anEka kategaLu kannaDada vividha niyatakAlikegaLalli prakaTavAgive
tiLidiruva ellA kaipar paTTi kAyagaLa nakZhe
puttUru dakZhiNa kannaDa jilleya oMdu pramuKa tAlUku kEMdra
karnATaka sarakArada ArOgya mattu kuTuMba kalyANa ilAKeyalli sEve sallisuttiddAre
BAratada sEnA puraskAragaLu padavannu bEre lEKanagaLalli huDuki
idu dakZhiNa kannaDa jilleyalli ati vistAravAda tAlUku
karnATaka da uttara BAgadalliruva pramuKa vANijya nagara
cAlukya vaMSada ati prasidda cakravarti
idannu paryAya nObel eMdU kareyalAguttade
BAratIya sEne hesarina lEKanavannu nIvu prAraMBisabahudu
avaru oTTu hannoMdu kavana saMkalanagaLannu prakaTisiddAre
arthaSAstra dalli snAtakOttara padavi paDediddAre
kannaDa iMglIZh eraDU BAZhegaLalli gOkAkaru halavAru kRutigaLannu racisiddAre
nArAyaNa avaru I kelasavannu mADidaru
karnATaka da rAjadhAni beMgaLUru nagarada bagge lEKanagaLu
pArsigaLu kraista ru muslimaru mattu yahUdi gaLu illi nelesidaru
Adare idu jvAlAmuKiya oMdu bage mAtra
BAratIya sEne padavannu bEre lEKanagaLalli huDuki
kannaDa mattu saMskRuta eraDU BAZhegaLalli vAdirAjaru kRutigaLannu racisiddAre
laMkES avaru idara sthApaka saMpAdakaru
I kOD saMKyeyannu kOD pAyiMT ennuttAre
mADida ivaru AkASavANiyalli sEve sallisuttiddAre
uttara karnATakadalli kaMDu baruva sAmAnya padagaLannu gamanisi
BAZhA kuTuMba kke sErida oMdu BAZhe
neharu ravara snEha avarige doreyitu
dhAravADa da samAja kke maraLidaru
BArata da moTTamodala railu muMbayi nalli ODitu
idu bahaLa vEgavAgi beLeyuva mara
BAratada svAtaMtrya hesarina lEKanavannu nIvu prAraMBisabahudu
idu cikkamagaLUru jilleya pramuKa tAlUku
sAMskRutika viZhayagaLalli nEpALavu TibeT mattu BAratavannu hOluttade
kabbiNa doMdige sErisi nitya baLakeyalliruva ukku nnu tayArisuttAre
naTarAj iMgliZh snAtakOttara adhyayana mADida sAhityada vidyArthi
I lEKanadalli eraDU padagaLannu baLasalAgide
modalige kannaDa citraraMgadalli yaSasvi citragaLannu nirdESisida ji ayyar
anaMta nAg nAyakana pAtradalli aBinayisiddAre
BAratada svAtaMtrya padavannu bEre lEKanagaLalli huDuki
nArAyaNa ru kannaDa da hiriya jAnapada vidvAMsaralli obbaru
muMde nail nadiyu suDAn nalli hariyuttade
BArata dESada karnATaka rAjyada oMdu nagara
ivaru kannaDa hAgU AMgla BAZhegaLalli sAhitya racisiddAre
BAratada nAlku cakra vAhanagaLu hesarina lEKanavannu nIvu prAraMBisabahudu
uttara kannaDa jilleya oMdu tAlUku mattu jillA kEMdra
idariMdAgi avara saMgItakke hosadoMdu guNa laBisitu
pUrNacaMdra tEjasvi avara kRuti AdhArita citra
I kAraNadiMda Sukravannu BUmiya avaLi eMdU kareyuttAre
karnATakadalli malenADu pradESadalli heccAgi kaMDu baruvudu
kalyANi cAlukya ra dakZhiNa BAgada nADina rAjadhAniyAgittu baLLigAvi
kathe kavana vimarSe hosa pustaka paricaya makkaLa puTa janapriyavAgive
yOga hesarina lEKanavannu nIvu prAraMBisabahudu
BArata dESada karnATaka rAjyada maisUru jilleyalli oMdu nagara
BAratada nAlku cakra vAhanagaLu padavannu bEre lEKanagaLalli huDuki
rAmacaMdra Sarma kannaDada KyAta vidvAMsa mattu sAhiti
idu uDupi jilleya oMdu tAlUku kEMdra
kannaDa sAhitya pariZhattina adhyakZharAgi kelasa mADiddAre
idallade halavu saNNa dvIpagaLannu saha oLagoMDide
amErika saMyukta saMsthAna da rAZhTrapati gaLu amErika dESada sarakArada adhyakZharu
ivaru anEka hiMdi kRutigaLannu kannaDa kke anuvAdisiddAre
kannaDadalli KaMDa ennuva pada baLakeyallide
yOga padavannu bEre lEKanagaLalli huDuki
Adare vidyunmAna alegaLa mEle I padarada praBAva kaDime
avaru huTTida maneyannu iMdigU muMbai na je
laMkES patrike mUlaka baravaNige AraMBisidaru
bayalu dAri idE hesariniMda calanacitravAgide
neraLu nATakakke rAjya sAhitya akADemiya bahumAna laBiside
dhUmakEtu KagOLa vastu dhUmakEtu calanacitra
Adare matte sAhitigaLa viBAgadalli nODidAga vinAyaka kRuZhNa gOkAka lEKana kaMDu baMditu
Ti en sItArAM kannaDada janapriya nirdESakarallobbaru
calanacitra hesarina lEKanavannu nIvu prAraMBisabahudu
muKya puTada mEle prayOga mADalu I puTavannu upayOgisabahudu
SaMkaranAg aBinayada praprathama kannaDa calanacitra
kannaDa citraraMgada prasiddha nirdESakaralli obbaru
ravara salaheyaMte iMgleMD ge vaidyakIya sahAyakke hOdaru
calanacitra padavannu bEre lEKanagaLalli huDuki
idara carcApuTadalli ivugaLa bagge nIvu carce naDesabahudu
citradurga jilleya tAlUku gaLa bagge lEKanagaLu
SrI smAraka bahumAna hAgu karnATaka rAjya sAhitya akADemi puraskAra saMdive
dakZhiNa amerika KaMDada paScima BAgadalliruva oMdu rAZhTra
bi ji el svAmiyavara kRuti